﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class BookTypeTable
    {
        [Key]
        public int BookTypeID { get; set; }
        [Required(ErrorMessage = "Please Enter Book Type")]
        public string Name { get; set; }
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual UserTable UserTable { get; set; }
        public virtual ICollection<BookTable> BookTables { get; set; }

    }
}
