﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class UserTable
    {
        [Key]
        public int UserID { get; set; }
        [Required(ErrorMessage = "Please Enter User Name")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Please Enter Password")]
        public string Password { get; set; }
        [Display(Name ="Status")]
        public bool IsActive { get; set; }
        [Required(ErrorMessage = "Please Enter Employee")]
        public int EmployeeID { get; set; }
        [ForeignKey("EmployeeID")]
        public virtual EmployeeTable EmployeeTable { get; set; }
        [Required(ErrorMessage = "Please Enter User Type")]
        public int UsertypeID { get; set; }
        [ForeignKey("UsertypeID")]
        public virtual UserTypeTable UserTypeTable { get; set; }

        public virtual ICollection<SupplierTable> SupplierTables { get; set; }
        public virtual ICollection<BookTable> BookTables { get; set; }
        public virtual ICollection<BookFineTable> BookFineTables { get; set; }
        public virtual ICollection<DepartmentTable> DepartmentTables { get; set; }
        public virtual ICollection<PurchaseTable> PurchaseTables { get; set; }
        public virtual ICollection<IssueBookTable> IssueBookTables { get; set; }
        public virtual ICollection<BookReturn> BookReturns { get; set; }
        public virtual ICollection<DesignationTable> DesignationTables { get; set; }
    }
}
