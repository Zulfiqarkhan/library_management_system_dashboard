﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class BookTable
    {
        [Key]
        public int BookID { get; set; }
        [Required(ErrorMessage = "Please Enter Book Title")]
        public string BookTitle { get; set; }
        [Required(ErrorMessage = "Please Enter Book Name")]
        public string BookName { get; set; }
        [Required(ErrorMessage = "Please Enter Author Name")]
        public string Author { get; set; }
        [Required(ErrorMessage = "Please Enter Book Edition")]
        public float Edition { get; set; }
        [Required(ErrorMessage = "Please Enter Book Price")]
        public decimal Price { get; set; }
        [Required(ErrorMessage = "Please Enter Total Stock")]
        public decimal TotalCopies { get; set; }
        public DateTime RegDate { get; set; }
        public string Description { get; set; }
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual UserTable UserTable { get; set; }
        [Required(ErrorMessage = "Please Enter Department")]
        public int DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public virtual DepartmentTable DepartmentTable { get; set; }
        [Required(ErrorMessage = "Please Enter BookType")]
        public int BookTypeID { get; set; }
        [ForeignKey("BookTypeID")]
        public virtual BookTypeTable BookTypeTable { get; set; }

        public virtual ICollection<IssueBookTable> IssueBookTables { get; set; }
        public virtual ICollection<BookFineTable> BookFineTables { get; set; }
        public virtual ICollection<BookReturn> BookReturnTables { get; set; }
        public virtual ICollection<PurchaseDetailsTable> PurchaseDetaisTables { get; set; }

        

    }
}
