﻿namespace LibraryMgtSystemDB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialize1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserTables", "EmployeeID", "dbo.EmployeeTables");
            AddForeignKey("dbo.UserTables", "EmployeeID", "dbo.EmployeeTables", "EmployeeID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserTables", "EmployeeID", "dbo.EmployeeTables");
            AddForeignKey("dbo.UserTables", "EmployeeID", "dbo.EmployeeTables", "EmployeeID", cascadeDelete: true);
        }
    }
}
