﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class UserTypeTable
    {
        [Key]
        public int UsertypeID { get; set; }
        [Required(ErrorMessage = "Please Enter User Type")]
        public string Usertype { get; set; }
        public virtual ICollection<UserTable> UserTables { get; set; }
    }
}
