﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class DepartmentTable
    {
        [Key]
        public int DepartmentID { get; set; }
        [Required(ErrorMessage = "Please Enter Department Name")]
        public string DepartmentName { get; set; }
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual UserTable UserTable { get; set; }
        public virtual ICollection<BookTable> BookTables { get; set; }
        public virtual ICollection<EmployeeTable> EmployeeTables { get; set; }

    }
}
