﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class LMSContext : DbContext
    {
        public LMSContext() : base("LMSContext")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BookTable>()
                .HasRequired(s => s.UserTable)
                .WithMany(t => t.BookTables)
                .HasForeignKey(d => d.UserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DepartmentTable>()
             .HasRequired<UserTable>(s => s.UserTable)
              .WithMany(t => t.DepartmentTables)
                .HasForeignKey(d => d.UserID)
             .WillCascadeOnDelete(false);

            modelBuilder.Entity<SupplierTable>()
            .HasRequired<UserTable>(s => s.UserTable)
             .WithMany(t => t.SupplierTables)
                .HasForeignKey(d => d.UserID)
            .WillCascadeOnDelete(false);


            modelBuilder.Entity<IssueBookTable>()
            .HasRequired<UserTable>(s => s.UserTable)
             .WithMany(t => t.IssueBookTables)
                .HasForeignKey(d => d.UserID)
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<BookReturn>()
            .HasRequired<UserTable>(s => s.UserTable)
             .WithMany(t => t.BookReturns)
                .HasForeignKey(d => d.UserID)
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<DesignationTable>()
            .HasRequired<UserTable>(s => s.UserTable)
             .WithMany(t => t.DesignationTables)
                .HasForeignKey(d => d.UserID)
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<BookFineTable>()
            .HasRequired<UserTable>(s => s.UserTable)
             .WithMany(t => t.BookFineTables)
                .HasForeignKey(d => d.UserID)
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<PurchaseTable>()
            .HasRequired<UserTable>(s => s.UserTable)
            .WithMany(t => t.PurchaseTables)
                .HasForeignKey(d => d.UserID)
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<EmployeeTable>()
              .HasRequired<DepartmentTable>(s => s.DepartmentTable)
             .WithMany(t => t.EmployeeTables)
                .HasForeignKey(d => d.DepartmentID)
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserTable>()
            .HasRequired<EmployeeTable>(s => s.EmployeeTable)
            .WithMany(t => t.UserTables)
              .HasForeignKey(d => d.EmployeeID)
             .WillCascadeOnDelete(false);
        }
        public DbSet<BookFineTable> BookFineTables { get; set; }
        public DbSet<BookReturn> BookReturns { get; set; }
        public DbSet<BookTable> BookTables { get; set; }
        public DbSet<BookTypeTable> BookTypeTables { get; set; }
        public DbSet<DepartmentTable> DepartmentTables { get; set; }
        public DbSet<DesignationTable> DesignationTables { get; set; }
        public DbSet<EmployeeTable> EmployeeTables { get; set; }
        public DbSet<IssueBookTable> IssueBookTables { get; set; }
        public DbSet<PurchaseDetailsTable> PurchaseDetailsTables { get; set; }
        public DbSet<PurchaseTable> PurchaseTables { get; set; }
        public DbSet<PurTemDetailsTable> PurTemDetailsTables { get; set; }
        public DbSet<SupplierTable> SupplierTables { get; set; }
        public DbSet<UserTable> UserTables { get; set; }
        public DbSet<UserTypeTable> UserTypeTables { get; set; }

    }

}
