﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
   public class PurchaseTable
    {
        [Key]
        public int PurchaseID { get; set; }
        [Required(ErrorMessage = "Please Enter Purchase Date")]
        [DataType(DataType.Date)]
        public DateTime PurchaseDate { get; set; }
        [Required(ErrorMessage = "Please Enter Purchase Amount")]
        public decimal PurchaseAmount { get; set; }
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual UserTable UserTable { get; set; }
        [Required(ErrorMessage = "Please Enter Supplier")]
        public int SupplierID { get; set; }
        [ForeignKey("SupplierID")]
        public virtual SupplierTable SupplierTable { get; set; }
        public virtual ICollection<PurchaseDetailsTable> PurchaseDetailsTables { get; set; }

    }
}
