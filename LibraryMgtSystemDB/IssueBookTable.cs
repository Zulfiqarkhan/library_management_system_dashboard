﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class IssueBookTable
    {
        [Key]
        public int IssueBookID { get; set; }
        [Required(ErrorMessage = "Please Enter Issue Copies Number")]
        public int NoOfCopies { get; set; }
        [Required(ErrorMessage = "Please Enter Issue Date")]
        [DataType(DataType.Date)]
        public DateTime IssueDate { get; set; }
        [Required(ErrorMessage = "Please Enter Return Date")]
        [DataType(DataType.Date)]
        public DateTime ReturnDate { get; set; }
        [Display(Name ="Status")]
        public bool Status { get; set; }
        public bool ReserveCopies { get; set; }
        public string Description { get; set; }
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual UserTable UserTable { get; set; }
        [Required(ErrorMessage = "Please Enter Book")]
        public int BookID { get; set; }
        [ForeignKey("BookID")]
        public virtual BookTable BookTable { get; set; }
        [Required(ErrorMessage = "Please Enter Employee")]
        public int EmployeeID { get; set; }
        [ForeignKey("EmployeeID")]
        public virtual EmployeeTable EmployeeTable { get; set; }


    }
}
