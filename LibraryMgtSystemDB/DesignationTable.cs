﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class DesignationTable
    {
        [Key]
        public int DesignationID { get; set; }
        [Required(ErrorMessage = "Please Enter Designation")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please Enter Scale")]
        public int Scale { get; set; }
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual UserTable UserTable { get; set; }
        public virtual ICollection<EmployeeTable> EmployeeTables { get; set; }
    }
}
