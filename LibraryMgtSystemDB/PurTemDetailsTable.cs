﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class PurTemDetailsTable
    {
        [Key]
        public int PurTemDetailsID { get; set; }
        public int Qty { get; set; }
        public int UnitPrice { get; set; }
        public int BookID { get; set; }
        [ForeignKey("BookID")]
        public virtual BookTable BookTable { get; set; }
    }
}
