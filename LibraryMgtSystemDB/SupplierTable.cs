﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class SupplierTable
    {
        [Key]
        public int SupplierID { get; set; }
        [Required(ErrorMessage = "Please Enter Supplier Name")]
        public string SupplierName { get; set; }
        [Required(ErrorMessage = "Please Enter Contact Number")]
        public int ContactNo { get; set; }
        [Required(ErrorMessage = "Please Enter Email")]
        public string Email { get; set; }
        public string Description { get; set; }
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual UserTable UserTable { get; set; }
        public virtual ICollection<PurchaseTable> PurchaseTables { get; set; }
    }
}
