﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class EmployeeTable
    {
        [Key]
        public int EmployeeID { get; set; }
        [Required(ErrorMessage = "Please Enter EmployeeName")]
        public string EmployeeName { get; set; }
        [Required(ErrorMessage = "Please Enter Employee Father Name")]
        public string FatherName { get; set; }
        [Required(ErrorMessage = "Please Enter Contact Number")]
        public int ContactNo { get; set; }
        [Required(ErrorMessage = "Please Enter Email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please Enter Address")]
        public string Address { get; set; }
        public string Description { get; set; }
        [Display(Name ="Status")]
        public bool IsActive { get; set; }
        [Required(ErrorMessage = "Please Enter Department")]
        public int DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public virtual DepartmentTable DepartmentTable { get; set; }
        [Required(ErrorMessage = "Please Enter Designation")]
        public int DesignationID { get; set; }
        [ForeignKey("DesignationID")]
        public virtual DesignationTable DesignationTable { get; set; }
        public virtual ICollection<BookReturn> BookReturns { get; set; }
        public virtual ICollection<BookFineTable> BookFineTables { get; set; }
        public virtual ICollection<IssueBookTable> IssueBookTables { get; set; }
        public virtual ICollection<UserTable> UserTables { get; set; }
    }
}
