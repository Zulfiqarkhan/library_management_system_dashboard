﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class BookReturn
    {
        [Key]
        public int BookReturnID { get; set; }
        [Required(ErrorMessage = "Please Enter Issue Date")]
        [DataType(DataType.Date)]
        public DateTime IssueDate { get; set; }
        [Required(ErrorMessage = "Please Enter Return Date")]
        [DataType(DataType.Date)]
        public DateTime ReturnDate { get; set; }
        public DateTime CurrentDate { get; set; }
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual UserTable UserTable { get; set; }
        [Required(ErrorMessage = "Please Enter Book")]
        public int BookID { get; set; }
        [ForeignKey("BookID")]
        public virtual BookTable BookTable { get; set; }
        [Required(ErrorMessage = "Please Enter Employee")]
        public int EmployeeID { get; set; }
        [ForeignKey("EmployeeID")]
        public virtual EmployeeTable EmployeeTable { get; set; }
    }
}
