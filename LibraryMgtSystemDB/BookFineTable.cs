﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMgtSystemDB
{
    public class BookFineTable
    {
        [Key]
        public int BookFineID { get; set; }
        [Required(ErrorMessage ="Please Enter Fine Date")]
        [DataType(DataType.Date)]
        public DateTime FineDate { get; set; }
        [Required(ErrorMessage = "Please Enter Fine Panelty")]
        public decimal FineAmount { get; set; }
        [Required(ErrorMessage = "Please Enter Receive Fine")]
        public decimal  ReceiveAmount { get; set; }
        [Required(ErrorMessage = "Please Enter Fine No Of Days")]
        public int NoOfDays { get; set; }
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual UserTable UserTable { get; set; }
        [Required(ErrorMessage = "Please Enter Book")]
        public int BookID { get; set; }
        [ForeignKey("BookID")]
        public virtual BookTable BookTable { get; set; }
        [Required(ErrorMessage = "Please Enter Employee")]
        public int EmployeeID { get; set; }
        [ForeignKey("EmployeeID")]
        public virtual EmployeeTable EmployeeTable { get; set; }
    }
}
