﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibraryMgtSystemDB;

namespace Library_Management_System.web.Controllers
{
    public class IssueBookTablesController : Controller
    {
        private LMSContext db = new LMSContext();

        // GET: IssueBookTables
        public ActionResult Issuebooks()
        {
            var issueBookTables = db.IssueBookTables.Include(i => i.BookTable).Include(i => i.EmployeeTable).Include(i => i.UserTable).Where(x=>x.Status == true);
            return View(issueBookTables.ToList());
        }

        public ActionResult Reservebooks()
        {
            var issueBookTables = db.IssueBookTables.Include(i => i.BookTable).Include(i => i.EmployeeTable).Include(i => i.UserTable).Where(x => x.ReserveCopies == true);
            return View(issueBookTables.ToList());
        }

        public ActionResult ReturnPending()
        {
            var issueBookTables = db.IssueBookTables.Where(x=>x.Status == true || x.ReserveCopies == true).ToList();
            List<IssueBookTable> list = new List<IssueBookTable>();
            foreach (var item in issueBookTables)
            {
                var returndate = item.ReturnDate;
                int Noofdays = (returndate - DateTime.Now.Date).Days;
                if(Noofdays <= 3)
                {
                    list.Add(new IssueBookTable
                    {
                        IssueBookID = item.IssueBookID,
                        BookID = item.BookID,
                        BookTable = item.BookTable,
                        EmployeeID = item.EmployeeID,
                        EmployeeTable = item.EmployeeTable,
                        UserID = item.UserID,
                        UserTable =item.UserTable,
                        NoOfCopies = item.NoOfCopies,
                        IssueDate = item.IssueDate,
                        ReturnDate = item.ReturnDate,
                        Status = item.Status, 
                        ReserveCopies =item.ReserveCopies,
                        Description = item.Description,
                    });
                    
                }
                
            }

            return View(list.ToList());
        }

        // GET: IssueBookTables/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IssueBookTable issueBookTable = db.IssueBookTables.Find(id);
            if (issueBookTable == null)
            {
                return HttpNotFound();
            }
            return View(issueBookTable);
        }

        // GET: IssueBookTables/Create
        public ActionResult Create()
        {
            ViewBag.BookID = new SelectList(db.BookTables, "BookID", "BookTitle");
            ViewBag.EmployeeID = new SelectList(db.EmployeeTables, "EmployeeID", "EmployeeName");
            ViewBag.UserID = new SelectList(db.UserTables, "UserID", "UserName");
            return View();
        }

        // POST: IssueBookTables/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IssueBookTable issueBookTable)
        {
            if (ModelState.IsValid)
            {
                var issuebook = db.IssueBookTables.Where(x => x.ReturnDate > DateTime.Now && x.BookID == issueBookTable.BookID).ToList();
                int issuebookcount = 0;
                foreach (var item in issuebook)
                {
                    issuebookcount = issuebookcount + item.NoOfCopies;
                }

                var findbook = db.BookTables.Where(x => x.BookID == issueBookTable.BookID).FirstOrDefault();
                decimal bookstocks = findbook.TotalCopies;
                if(issuebookcount + issueBookTable.NoOfCopies > bookstocks)
                { 
                    if(issuebookcount >= bookstocks)
                    {
                    ViewBag.message = "Stock is Empty...";
                    return RedirectToAction("Issuebooks");
                    }
                       ViewBag.message = "Stock is less than..."+ issueBookTable.NoOfCopies ;
                       return RedirectToAction("Create");
                }

                db.IssueBookTables.Add(issueBookTable);
                db.SaveChanges();
                return RedirectToAction("Issuebooks");
            }

            ViewBag.BookID = new SelectList(db.BookTables, "BookID", "BookTitle", issueBookTable.BookID);
            ViewBag.EmployeeID = new SelectList(db.EmployeeTables, "EmployeeID", "EmployeeName", issueBookTable.EmployeeID);
            ViewBag.UserID = new SelectList(db.UserTables, "UserID", "UserName", issueBookTable.UserID);
            return View(issueBookTable);
        }

        // GET: IssueBookTables/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IssueBookTable issueBookTable = db.IssueBookTables.Find(id);
            if (issueBookTable == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookID = new SelectList(db.BookTables, "BookID", "BookTitle", issueBookTable.BookID);
            ViewBag.EmployeeID = new SelectList(db.EmployeeTables, "EmployeeID", "EmployeeName", issueBookTable.EmployeeID);
            ViewBag.UserID = new SelectList(db.UserTables, "UserID", "UserName", issueBookTable.UserID);
            return View(issueBookTable);
        }

        // POST: IssueBookTables/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IssueBookID,NoOfCopies,IssueDate,ReturnDate,Status,ReserveCopies,Description,UserID,BookID,EmployeeID")] IssueBookTable issueBookTable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(issueBookTable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookID = new SelectList(db.BookTables, "BookID", "BookTitle", issueBookTable.BookID);
            ViewBag.EmployeeID = new SelectList(db.EmployeeTables, "EmployeeID", "EmployeeName", issueBookTable.EmployeeID);
            ViewBag.UserID = new SelectList(db.UserTables, "UserID", "UserName", issueBookTable.UserID);
            return View(issueBookTable);
        }

        // GET: IssueBookTables/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IssueBookTable issueBookTable = db.IssueBookTables.Find(id);
            if (issueBookTable == null)
            {
                return HttpNotFound();
            }
            return View(issueBookTable);
        }

        // POST: IssueBookTables/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IssueBookTable issueBookTable = db.IssueBookTables.Find(id);
            db.IssueBookTables.Remove(issueBookTable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult returnbook(int? id)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }
            int userid = Convert.ToInt32(Convert.ToString(Session["userid"]));

            var book = db.IssueBookTables.Find(id);
            int fine = 0;
            var returndate = book.ReturnDate;
            int noofdays = (DateTime.Now.Date - returndate).Days;
            if(book.Status == true && book.ReserveCopies == false)
            {
                if(noofdays > 0)
                {
                    fine = noofdays * 20;
                }
                var returnbook = new BookReturn
                {
                    IssueDate = book.IssueDate,
                    ReturnDate = book.ReturnDate,
                    CurrentDate = DateTime.Now,
                    BookID = book.BookID,
                    EmployeeID = book.EmployeeID,
                    UserID = book.UserID,

                };
                db.BookReturns.Add(returnbook);
                db.SaveChanges();
            }

            book.Status = false;
            book.ReserveCopies = false;
            db.Entry(book).State = EntityState.Modified;
            db.SaveChanges();

            if(fine > 0)
            {
                var addfine = new BookFineTable
                {
                    BookID = book.BookID,
                    EmployeeID = book.EmployeeID,
                    FineAmount = fine,
                    FineDate = DateTime.Now,
                    NoOfDays = noofdays,
                    ReceiveAmount = 0,
                    UserID = userid,
                };
                db.BookFineTables.Add(addfine);
                db.SaveChanges();
            }


            return RedirectToAction("Issuebooks");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
