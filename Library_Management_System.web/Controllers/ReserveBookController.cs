﻿using LibraryMgtSystemDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library_Management_System.web.Controllers
{
    public class ReserveBookController : Controller
    {
        LMSContext db = new LMSContext();
        // GET: ReserveBook
        public ActionResult Index()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }
            var reservelist = db.BookTables.ToList();
            return View(reservelist);
        }

        public ActionResult Reservebook(int? id)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }
            var userid = Convert.ToInt32(Session["userid"]);
            var employeeid = Convert.ToInt32(Session["employeeid"]);
            var book = db.BookTables.Find(id);
            var issuebook = db.IssueBookTables.Where(x => x.ReturnDate > DateTime.Now && x.BookID == id).ToList();

            int issuebookcount = 0;
            foreach (var item in issuebook)
            {
                issuebookcount = issuebookcount + item.NoOfCopies;
            }

            //var findbook = db.BookTables.Where(x => x.BookID == issueBookTable.BookID).FirstOrDefault();

            var issuebooktable = new IssueBookTable()
            {
                BookID = book.BookID,
                NoOfCopies = 1,
                IssueDate = DateTime.Now,
                ReturnDate = DateTime.Now.AddDays(2),
                Description = "Reserve",
                Status = false,
                ReserveCopies = true,
                UserID = userid,
                EmployeeID = employeeid,
            };

            decimal bookstocks = book.TotalCopies;

            if (issuebookcount >= bookstocks)
            {
                ViewBag.message = "Stock is Empty...";
                return RedirectToAction("index");
            }

            db.IssueBookTables.Add(issuebooktable);
            db.SaveChanges();
            ViewBag.message = "Successfully Reserved...";
            return RedirectToAction("index");
        }

        public ActionResult ApproveBook(int? id)
        {
            var findbook = db.IssueBookTables.Find(id);
            findbook.ReserveCopies = false;
            findbook.Status = true;
            db.Entry(findbook).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Issuebooks","Issuebooktables");
        }
    }
}
