﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibraryMgtSystemDB;
using Library_Management_System.web.Models;

namespace Library_Management_System.web.Controllers
{
    public class PurchaseTablesController : Controller
    {
        decimal totalamount = 0;
        private LMSContext db = new LMSContext();

        // GET: PurchaseTables
        public ActionResult NewPurchase()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }
            decimal totalamount = 0;
            var purlist = db.PurTemDetailsTables.ToList();
            foreach(var item in purlist)
            {
                totalamount += (item.Qty * item.UnitPrice);
            }
            ViewBag.TotalAmount = totalamount;
            return View(purlist);
        }

        [HttpPost]
        public ActionResult Additem(int BID, int Qty, int price)
        {
           
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }
            var find = db.PurTemDetailsTables.Where(p=>p.BookID == BID).FirstOrDefault();
            if (find == null)
            {
               if( BID > 0 && Qty > 0 && price > 0)
                {
                    var newtiem = new PurTemDetailsTable()
                    {
                        BookID = BID,
                        Qty = Qty,
                        UnitPrice = price,
                    };

                    db.PurTemDetailsTables.Add(newtiem);
                    db.SaveChanges();
                    ViewBag.message = "Purchase Item Added Successfully.....";
                }
            }
            else
            {
                ViewBag.warning = "Already Exist Please Check.....";
               
                var purtemlist = db.PurTemDetailsTables.ToList();
                foreach (var item in purtemlist)
                {
                    totalamount += (item.Qty * item.UnitPrice);
                }
                ViewBag.TotalAmount = totalamount;
                return View("NewPurchase", purtemlist);
            }
            var purlist = db.PurTemDetailsTables.ToList();
            foreach (var item in purlist)
            {
                totalamount += (item.Qty * item.UnitPrice);
            }
            ViewBag.TotalAmount = totalamount;
            return View("NewPurchase", purlist);
        }

        public ActionResult GetBooks()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }
            List<BookMV> list = new List<BookMV>();
            var booklist = db.BookTables.ToList();
            foreach (var item in booklist)
            {
                list.Add(new BookMV { BookName = item.BookName, BookID = item.BookID });
            }
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }


        //POST: PurchaseTables/Delete/5
        [ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }
            PurTemDetailsTable purtemdetails = db.PurTemDetailsTables.Find(id);
            db.PurTemDetailsTables.Remove(purtemdetails);
            db.SaveChanges();
            ViewBag.delete = "Deleted Successfully.....";
            var purlist = db.PurTemDetailsTables.ToList();
            foreach (var item in purlist)
            {
                totalamount += (item.Qty * item.UnitPrice);
            }
            ViewBag.TotalAmount = totalamount;
            return View("NewPurchase", purlist);
        }



      
        public ActionResult CancelPurchase()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }

            var list = db.PurTemDetailsTables.ToList();
            var cancelstatus = false;
            foreach(var item in list)
            {
                db.Entry(item).State = EntityState.Deleted;
               int noofrecord = db.SaveChanges();
                   if(cancelstatus == false)
                {
                    if(noofrecord > 0)
                    {
                        cancelstatus = true;
                    }
                }
            }
            if (cancelstatus == true)
            {
                ViewBag.cancel = "Purchase's Cancel Successfully.......";
                var purlist = db.PurTemDetailsTables.ToList();
                return RedirectToAction("NewPurchase", purlist);
            }
            return RedirectToAction("NewPurchase");
        }

        public ActionResult SelectSupplier()
        {
            var purchasetemlist = db.PurTemDetailsTables.ToList();
            
            if (purchasetemlist.Count == 0)
            {
                ViewBag.purtemmessage = "Purchase Cart Is Empty....";
                var purlist = db.PurTemDetailsTables.ToList();
                return RedirectToAction("NewPurchase", purlist);
            }
            var supplierlist = db.SupplierTables.ToList();
            return View(supplierlist);
        }

     
        public ActionResult PurchaseConfirm(FormCollection collection)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }
            int userid = Convert.ToInt32(Convert.ToString(Session["userid"]));
            int supplierid = 0;
            string[] key = collection.AllKeys;
            foreach(var name in key)
            {
                if (name.Contains("name"))
                {
                string nameid = name;
                string[] valueid = nameid.Split(' ');
                supplierid = Convert.ToInt32(valueid[1]);
                }
            }

            var purtemdetails = db.PurTemDetailsTables.ToList();
            decimal totalamount = 0;
            foreach (var item in purtemdetails)
            {
                totalamount = totalamount + (item.Qty * item.UnitPrice);
            }
            if(totalamount == 0)
            {
                ViewBag.message = "Purchase Cart Empty....";
                var purlist = db.PurTemDetailsTables.ToList();
                return RedirectToAction("NewPurchase", purlist);
            }

            var purchasetable = new PurchaseTable()
            {
                SupplierID = supplierid,
                UserID = userid,
                PurchaseDate = DateTime.Now,
                PurchaseAmount = totalamount,
            };
            db.PurchaseTables.Add(purchasetable);
            db.SaveChanges();

            foreach(var item in purtemdetails)
            {
                var purchasedetails = new PurchaseDetailsTable()
                {
                    PurchaseID = purchasetable.PurchaseID,
                    BookID = item.BookID,
                    Qty = item.Qty,
                    UnitPrice = item.UnitPrice,
                };
                db.PurchaseDetailsTables.Add(purchasedetails);
                db.SaveChanges();

                var updatestock = db.BookTables.Find(item.BookID);
                if(updatestock != null)
                {
                    updatestock.TotalCopies = updatestock.TotalCopies + item.Qty;
                    updatestock.Price = item.UnitPrice;

                    db.Entry(updatestock).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

            var list = db.PurTemDetailsTables.ToList();
            var cancelstatus = false;
            foreach (var item in list)
            {
                db.Entry(item).State = EntityState.Deleted;
                int noofrecord = db.SaveChanges();
                if (cancelstatus == false)
                {
                    if (noofrecord > 0)
                    {
                        cancelstatus = true;
                    }
                }
            }
            if (cancelstatus == true)
            {
                ViewBag.purchaseconfirm = "Purchase's Cancel Successfully.......";
                //var purlist = db.PurTemDetailsTables.ToList();
                //return RedirectToAction("NewPurchase", purlist);
            }



            return RedirectToAction("AllPurchase");
        }

        public ActionResult AllPurchase()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }
            var list = db.PurchaseTables.ToList();
            return View(list);
        }

        public ActionResult PurchaseDetail()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }
            var list = db.PurchaseDetailsTables.ToList();
            return View(list);
        }

        //// GET: PurchaseTables/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }
        //    if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    PurchaseTable purchaseTable = db.PurchaseTables.Find(id);
        //    if (purchaseTable == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(purchaseTable);
        //}



        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
