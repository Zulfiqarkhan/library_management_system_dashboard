﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibraryMgtSystemDB;

namespace Library_Management_System.web.Controllers
{
    public class BookReturnsController : Controller
    {
        private LMSContext db = new LMSContext();

        // GET: BookReturns
        public ActionResult Bookreturnhistory()
        {
            var bookReturns = db.BookReturns.Include(b => b.BookTable).Include(b => b.EmployeeTable).Include(b => b.UserTable);
            return View(bookReturns.ToList());
        }

        public ActionResult Pendingfine()
        {
            var pendingfine = db.BookFineTables.Where(x=>x.ReceiveAmount == 0).ToList();

            return View(pendingfine.ToList());
        }

        public ActionResult SubmitFine(int? id)
        {
            var fine = db.BookFineTables.Find(id);
            fine.ReceiveAmount = fine.FineAmount;
            fine.FineDate = DateTime.Now;
            db.Entry(fine).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("pendingfine");
        }

        public ActionResult Bookfinehistory()
        {
            var finehistory = db.BookFineTables.Where(x => x.ReceiveAmount > 0).ToList();
            return View(finehistory);
        }
       

        // GET: BookReturns/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    BookReturn bookReturn = db.BookReturns.Find(id);
        //    if (bookReturn == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(bookReturn);
        //}

        //// GET: BookReturns/Create
        //public ActionResult Create()
        //{
        //    ViewBag.BookID = new SelectList(db.BookTables, "BookID", "BookTitle");
        //    ViewBag.EmployeeID = new SelectList(db.EmployeeTables, "EmployeeID", "EmployeeName");
        //    ViewBag.UserID = new SelectList(db.UserTables, "UserID", "UserName");
        //    return View();
        //}

        //// POST: BookReturns/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "BookReturnID,IssueDate,ReturnDate,CurrentDate,UserID,BookID,EmployeeID")] BookReturn bookReturn)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.BookReturns.Add(bookReturn);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.BookID = new SelectList(db.BookTables, "BookID", "BookTitle", bookReturn.BookID);
        //    ViewBag.EmployeeID = new SelectList(db.EmployeeTables, "EmployeeID", "EmployeeName", bookReturn.EmployeeID);
        //    ViewBag.UserID = new SelectList(db.UserTables, "UserID", "UserName", bookReturn.UserID);
        //    return View(bookReturn);
        //}

        //// GET: BookReturns/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    BookReturn bookReturn = db.BookReturns.Find(id);
        //    if (bookReturn == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.BookID = new SelectList(db.BookTables, "BookID", "BookTitle", bookReturn.BookID);
        //    ViewBag.EmployeeID = new SelectList(db.EmployeeTables, "EmployeeID", "EmployeeName", bookReturn.EmployeeID);
        //    ViewBag.UserID = new SelectList(db.UserTables, "UserID", "UserName", bookReturn.UserID);
        //    return View(bookReturn);
        //}

        //// POST: BookReturns/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "BookReturnID,IssueDate,ReturnDate,CurrentDate,UserID,BookID,EmployeeID")] BookReturn bookReturn)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(bookReturn).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.BookID = new SelectList(db.BookTables, "BookID", "BookTitle", bookReturn.BookID);
        //    ViewBag.EmployeeID = new SelectList(db.EmployeeTables, "EmployeeID", "EmployeeName", bookReturn.EmployeeID);
        //    ViewBag.UserID = new SelectList(db.UserTables, "UserID", "UserName", bookReturn.UserID);
        //    return View(bookReturn);
        //}

        //// GET: BookReturns/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    BookReturn bookReturn = db.BookReturns.Find(id);
        //    if (bookReturn == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(bookReturn);
        //}

        //// POST: BookReturns/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    BookReturn bookReturn = db.BookReturns.Find(id);
        //    db.BookReturns.Remove(bookReturn);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

     
    }
}
