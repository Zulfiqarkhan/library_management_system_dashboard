﻿using LibraryMgtSystemDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library_Management_System.web.Controllers
{
    public class HomeController : Controller
    {
        LMSContext db = new LMSContext();
        public ActionResult Index()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }


        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Login(string username, string password)
        {

            if (username != null && password != null)
            {
                try { 
                var loginuser = db.UserTables.Where(m => m.UserName == username && m.Password == password && m.IsActive == true).FirstOrDefault();
                 if(loginuser != null)
                {
                    Session["userid"] = loginuser.UserID;
                    Session["username"] = loginuser.UserName;
                    Session["password"] = loginuser.Password;
                    Session["employeeid"] = loginuser.EmployeeID;
                    Session["usertypeid"] = loginuser.UsertypeID;

                    if(loginuser.UsertypeID == 1)
                    {
                        return RedirectToAction("about");
                    }

                    if (loginuser.UsertypeID == 2)
                    {
                        return RedirectToAction("about");
                    }

                    if (loginuser.UsertypeID == 3)
                    {
                        return RedirectToAction("about");
                    }
                }
                else
                {
                    Session["userid"] =   string.Empty;
                    Session["username"] = string.Empty;
                    Session["password"] = string.Empty;
                    Session["employeeid"] = string.Empty;
                    Session["usertypeid"] = string.Empty;
                    ViewBag.message = "Please Enter Correct User Name and Password";
                }
                }
                catch(Exception ex)
                {
                    Session["userid"] = string.Empty;
                    Session["username"] = string.Empty;
                    Session["password"] = string.Empty;
                    Session["employeeid"] = string.Empty;
                    Session["usertypeid"] = string.Empty;
                    ViewBag.error = ex.Message;
                }

            }




            return View();
        }




        public ActionResult About()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }

             int usertypeid = 0;
            if (Session["usertypeid"] != null)
            {
                usertypeid = Convert.ToInt32(Session["usertypeid"]);
            }
            if(usertypeid == 2)
            {
                return RedirectToAction("index", "reservebook");
            }

            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["userid"])))
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult Logout()
        {
            Session["userid"] = string.Empty;
            Session["username"] = string.Empty;
            Session["password"] = string.Empty;
            Session["employee"] = string.Empty;
            Session["usertype"] = string.Empty;
            return RedirectToAction("Login", "Home");
        }
    }
}