﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library_Management_System.web.Models
{
    public class BookMV
    {
        public int BookID { get; set; }
        public string BookTitle { get; set; }
        public string BookName { get; set; }
        public string Author { get; set; }
        public float Edition { get; set; }
        public decimal Price { get; set; }
        public decimal TotalCopies { get; set; }
        public DateTime RegDate { get; set; }
        public string Description { get; set; }
        public int UserID { get; set; }
        public int DepartmentID { get; set; }
        public int BookTypeID { get; set; }
    }
}